import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UpdateUserDto } from 'src/users/dtos/update-user.dto';
import { CreateUserDto } from 'src/users/dtos/create-user.dto';
import { CreateUserPostDto } from 'src/users/dtos/create-user-post.dto';
import { CreateUserProfileDto } from 'src/users/dtos/create-user-profile.dto';
import { User } from 'src/users/schemas/users.schema';
import { UsersService } from 'src/users/services/users/users.service';

@Controller('users')
export class UsersController {

    constructor(private userService: UsersService) { }

    @Get()
    async getUsers(): Promise<User[]> {
        return this.userService.findUsers();
    }

    @Post('new')
    async createUser(@Body()
    user: CreateUserDto): Promise<User> {
        return this.userService.createUser(user);
    }

    @Get(':id')
    async getUser(@Param('id') id: string): Promise<User> {
        return this.userService.findById(id);
    }

    @Put(':id')
    async updateUser(
        @Param('id')
        id: string,
        @Body()
        user: UpdateUserDto): Promise<User> {
        return this.userService.updateUserById(id, user);
    }

    @Delete(':id')
    async deleteUser(
        @Param('id')
        id: string) {
        this.userService.deleteUserById(id);
    }

    // @Post(':id/profiles')
    // createUserProfile(
    //     @Param('id', ParseIntPipe) id: number,
    //     @Body() createUserProfileDto: CreateUserProfileDto
    // ) {
    //     return this.userService.createUserProfile(id, createUserProfileDto)
    // }

    // @Post(':id/posts')
    // createUserPost(
    //     @Param('id', ParseIntPipe) id: number,
    //     @Body() createUserPostDto: CreateUserPostDto
    // ) {
    //     return this.userService.createUserPost(id, createUserPostDto)
    // }

}
