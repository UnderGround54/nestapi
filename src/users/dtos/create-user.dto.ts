export class CreateUserDto {
    username: string;
    password: string;
    authstrategy: string;
}