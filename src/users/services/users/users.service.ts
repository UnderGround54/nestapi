import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { User } from 'src/users/schemas/users.schema';
//import { User } from 'src/typeorm/entities/Users';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {

    constructor(
        @InjectModel(User.name) private userModel: mongoose.Model<User>
    ) { }

    async findUsers(): Promise<User[]> {
        const users = await this.userModel.find();
        return users;
    }

   async createUser(user: User): Promise<User> {
        const res = await this.userModel.create(user);
        return res;
    }

    async findById(id: string): Promise<User> {
        const user = await this.userModel.findById(id);

        if(!user){
            throw new NotFoundException('User not found')
        }
        
        return user;
    }
    
    async updateUserById(id: string, user: User): Promise<User> {
        return await this.userModel.findByIdAndUpdate(id, user, {
            new: true,
            runValidators: true
        });
    }

    async deleteUserById(id: string): Promise<User> {
        return await this.userModel.findByIdAndDelete(id);
    }

    /*async createUserProfile(
        id: number,
        createUserProfileDetails: CreateUserProfileParams
    ){
        const user = await this.userRepository.findOneBy({ id })
        if(!user)
            throw new HttpException(
                'User not found',
                HttpStatus.BAD_REQUEST
            );
        
        const newProfile = this.profileRepository.create(createUserProfileDetails);
        const savedProfile =  await this.profileRepository.save(newProfile);
        user.profile = savedProfile;
        return this.userRepository.save(user)
    }

    async createUserPost(
        id:number,
        createUserPostDetails: CreateUserPostParams
    ){
        const user = await this.userRepository.findOneBy({ id })
        if(!user)
            throw new HttpException(
                'User not found',
                HttpStatus.BAD_REQUEST
            );
        const newPost = this.postRepository.create({
            ...createUserPostDetails,
            user,
            });
        return await this.postRepository.save(newPost);
    }*/
}
