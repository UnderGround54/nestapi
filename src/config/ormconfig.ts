import { MongooseModule } from "@nestjs/mongoose";


const config: MongooseModule = {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '',
    database: 'sirh-v3',
    entities:  ['dist/typeorm/entities/*.js'],
    synchronize: true
}

export default config;